# vim:ft=zsh ts=2 sw=2 sts=2

local return_code='%(?..%{$fg_bold[red]%}%? ↵%{$reset_color%})'

# Must use Powerline font, for \uE0A0 to render.
ZSH_THEME_GIT_PROMPT_PREFIX="( on %{$fg[yellow]%}\uE0A0 "
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%})"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%} ±"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[green]%} ?"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[green]%} ✓"

PROMPT='%{$fg_bold[green]%}%n@%m%{$reset_color%}:%{$fg_bold[blue]%}%~%{$reset_color%}$(git_prompt_info)%{$reset_color%}$ '
PROMPT+='
%(?:%{$fg_bold[green]%}➜ %{$reset_color%}:%{$fg_bold[red]%}✗ %{$reset_color%})'

RPROMPT="${return_code} ⌚ %{$fg[red]%}%*%{$reset_color%}"